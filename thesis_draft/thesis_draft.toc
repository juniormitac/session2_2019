\contentsline {chapter}{\numberline {1}Classical Compression Algorithms}{4}
\contentsline {section}{\numberline {1.1}Background}{4}
\contentsline {section}{\numberline {1.2}Huffman-Coding}{5}
\contentsline {subsection}{\numberline {1.2.1}Huffman-Codes}{6}
\contentsline {subsection}{\numberline {1.2.2}Properties of Huffman Codes}{11}
\contentsline {section}{\numberline {1.3}Integer Codes}{12}
\contentsline {section}{\numberline {1.4}Dictionary based encoding}{13}
\contentsline {subsection}{\numberline {1.4.1}Lempel-Ziv-Welch}{13}
\contentsline {subsection}{\numberline {1.4.2}Algorithm}{13}
\contentsline {subsection}{\numberline {1.4.3}History}{16}
\contentsline {chapter}{\numberline {2}Arithmetic coding}{17}
\contentsline {section}{\numberline {2.1}Background}{17}
\contentsline {section}{\numberline {2.2}Coding}{18}
\contentsline {section}{\numberline {2.3}Algorithm}{18}
\contentsline {chapter}{\numberline {3}Data transmission}{19}
\contentsline {section}{\numberline {3.1}Introduction}{19}
\contentsline {section}{\numberline {3.2}Background}{22}
\contentsline {section}{\numberline {3.3}Disk Image Structure}{24}
\contentsline {chapter}{\numberline {4}Algorithm}{25}
\contentsline {section}{\numberline {4.1}Dictionaries}{25}
\contentsline {section}{\numberline {4.2}Duplicated Data and Representation}{26}
\contentsline {section}{\numberline {4.3}Continuous Data Transmission}{26}
\contentsline {section}{\numberline {4.4}Compressed Data Transmission}{27}
\contentsline {section}{\numberline {4.5}The Scheme}{27}
\contentsline {chapter}{\numberline {5}Experiments}{28}
\contentsline {section}{\numberline {5.1}Method}{28}
\contentsline {section}{\numberline {5.2}Sample Data}{29}
\contentsline {section}{\numberline {5.3}Results}{30}
\contentsline {section}{\numberline {5.4}Result Discussion}{32}
\contentsline {chapter}{\numberline {6}Conclusion and Future Work}{33}
\contentsline {section}{\numberline {6.1}Introduction}{33}
\contentsline {section}{\numberline {6.2}Disk Images}{33}
\contentsline {section}{\numberline {6.3}Data Transmission}{34}
\contentsline {section}{\numberline {6.4}Background Work}{34}
\contentsline {section}{\numberline {6.5}Further Software Work}{34}
